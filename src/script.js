
let frames = []
let time = 0
let rate = 30
let currentframe = 0
let imagecount = 585
let lastframetime = 0

function getzeros(i, a){
  return i > 0 ? getzeros(i - 1, a + "0") : a
}
function addzeros(n){
  let s = n + ""
  if(s.length < 3) 
    s = getzeros(3 - s.length, "") + s
  return s
}
function preload(){
  for(var i = 0; i<imagecount; i++)
    frames.push(
      loadImage("media/images/Sequence 02" + addzeros(i) + ".png")
    )
}

function setup(){
  createCanvas(innerWidth, innerWidth * (9/16))
}

function draw(){
  let deltatime = (millis() - lastframetime) * 0.001
  let nt = (time + rate * deltatime)
  time = nt - floor(nt)
  if(floor(nt) > 0)
    currentframe = (currentframe + 1) % imagecount

  image(frames[currentframe], 0, 0, width, height)
  tint(255, 255 * time)
  image(frames[(currentframe + 1) % imagecount], 0, 0, width, height)

  lastframetime = millis()
}
